## Histórico de alterações do Manual do Usuário.

Data | Versão | Descrição | Autor
:-----:|:--------:|:-----------:|:------:
10/09/2017 | 0.0.1 | Inicio do documento. Especificações de instalações e comandos iniciais | Jerlianni Oliveira
12/09/2017 | 0.0.2 | Descrição do comando MODE. Pequenas alterações em algumas descrições | Jerlianni Oliveira
13/09/2017 | 0.0.3 | Melhoria visual do documento usando Markdown. Acréscimo do comando NAMES | Jerlianni Oliveira
14/09/2017 | 0.0.4 | Acréscimo do comando LIST, INVITE e KICK | Jerlianni Oliveira
15/09/2017 | 0.1.1 | Remoção dos tópicos em nivel de desenvolvedor. Recomeçarei o documento com o Chatzilla| Jerlianni Oliveira
19/09/2017 | 0.1.2 | Iniciando descrições para entrada no Chatzilla| Jerlianni Oliveira
21/09/2017 | 0.1.3 | Acréscimos de comandos e descrições| Jerlianni Oliveira
22/09/2017 | 0.1.4 | Acréscimos de prints e comandos e atualização do sumário| Jerlianni Oliveira
24/09/2017 | 0.2.0 | Últimos comandos e atualização do sumário| Jerlianni Oliveira