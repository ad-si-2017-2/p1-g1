# Manual do Usuário 

## Histórico de Alterações 

Para ver a descrição das alterações feitas neste documento e a versão atual,
<a href="https://gitlab.com/ad-si-2017-2/p1-g1/blob/anni/ManualUser/Hist%C3%B3rico%20de%20Altera%C3%A7%C3%B5es%20-%20User.md#hist%C3%B3rico-de-altera%C3%A7%C3%B5es-do-manual-do-usu%C3%A1rio">acesse aqui!</a>

## Sumário
* [Introdução] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualUser/#1-introdu%C3%A7%C3%A3o)
* [Como instalar]	(https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualUser/#2-como-instalar)
    * [Requisitos de Hardware e Software]	(https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualUser/#21-requisitos-de-hardware-e-software)
* [Usando o Chatzilla]	(https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualUser/#3-usando-o-chatzilla)
    * [JOIN - Criando/entrando em um canal]	(https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualUser/#31-join-criandoentrando-em-um-canal)
    * [Tópicos - Temas do canal] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualUser/#32-t%C3%B3picos-temas-do-canal)
    * [PART] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualUser/#33-part)
    * [QUIT] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualUser/#34-quit)
    * [LIST] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualUser/#35-list)
    * [NAMES] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualUser/#36-names)
    * [INVITE] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualUser/#37-invite)
    * [KICK] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualUser/#38-kick)
    * [WHOIS] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualUser/#39-whois)
    * [WHOWAS] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualUser/#310-whowas)


<div style="page-break-after: always;"></div>
*****

### 1. Introdução

<p>Este manual pretende ser um guia simples e visa demonstrar o que o usuário precisa referente a software para utilizar 
o servidor IRC desenvolvido pela nossa equipe.<br />

Foi criado para ser utilizado por grandes grupos corporativos, como um meio facilitador, 
sendo semelhante ao WhatsApp e Telegram, porém seu destaque está nas categorias de usuários.
</p> 
****

### 2. Como instalar
<p> Para utilizar o servidor IRC plataforma de execução javascript que atuará do lado servidor, Nodejs.<br />

O programa Nodejs pode ser encontrado no seu site oficial que pode ser encontrado pesquisando no Google, 
<a href="https://nodejs.org/en/download/">acessando aqui!</a><br />

No site tem os passos para fazer a instalação do programa, único requisito é saber qual seu sistema operacional 
e qual o seu processador (32 ou 64 bits).<br /><br />

<strong> Aconselhamos usar como teste o Chatzilla, uma extensão do Firefox. Por ter uma plataforma simples e de fácil integração. </strong>
Mas não está limitado ao Chatzilla, pode ser usado qualquer plataforma que usa servidor IRC para teste. Iremos ensinar como habilitar e testar o servidor
e suas funcionalidades pelo Chatzilla.<br />

O código do servidor IRC está disponível no repositório do gitlab: 
É bem simples sua funcionalidade do repositório, entre <a href="https://gitlab.com/ad-si-2017-2/p1-g1">acessando aqui!</a><br />

Agora só precisa fazer o download da pasta. Vá ao canto direito da tela e click no ícone de download, assim como mostrado na imagem: <br />
</p>

![image](/Imagens/2017-09-19_LI.jpg)

<p> Após o download, descompacte a pasta e deixe-a dentro da pasta de downloads, renomeando para <strong>"p1-g1"</strong>. 
Vá na busca de seu computador e digite <strong>"CMD"</strong> assim como na imagem e abra o promp de comando:
</p>

![image](Imagens/CMD.jpg)

<p> Com o prompt aberto, repita os comandos à seguir:
</p>

![image](Imagens/2017-09-21_LI.jpg)

<p> Com isso, o servidor IRC foi iniciado com sucesso! Assim como mostrado na terceira imagem, a porta e nome de servidor 
já estarão configurados para assim ficar mais fácil a configuração final.<br /><br />

E como já foi dito, o Chatzilla é uma extenção do Firefox. Será necessário a instalação do Firefox. <br />
Caso não tenha em sua máquina <a href="https://www.mozilla.org/pt-BR/firefox/new/">acesse aqui!</a><br /><br />

Com a instalação do Firefox, deve ser instalada a extenção do Chatzilla. Com o Firefox aberto, 
<a href="https://addons.mozilla.org/pt-BR/firefox/addon/chatzilla/">acesse aqui!</a> .<br /><br />

Com isso o Chatzilla está pronto para ser usado!
</p>

#### 2.1 Requisitos de Hardware e Software
<p> <strong>Plataforma Nodejs</strong>: Pode ser rodado em qualquer sistema operacional (SO), mas terá que possuir 125 MB de RAM.
</p>

****

### 3. Usando o Chatzilla
<p> O Chatzilla é bem simples de ser usado, não é necessário fazer login pois ele identifica a conta de sua máquina e faz seu cadastro.<br />
Com o servidor rodando, abra o Chatzilla no seu Firefox usando esse endereço **chrome://chatzilla/content/chatzilla.xul**.

Use esse comando <kbd>"/server 127.0.0.1 6667"</kbd>, o ChatZilla conectará com o servidor. Sua tela ficará assim: 
</p>

![image](Imagens/Chatzilla-serverRunning.png)


#### 3.1 JOIN - Criando/entrando em um canal
<p> O comando JOIN tem sua peculiaridade de ser usado tanto para abrir um canal ou entrar em canal existente. 
Caso for aberto um canal novo, o usuário será o Administrador do canal.<br />
Nome válido para o canal: <em> #,&,!,+ (Inicial), 50 caracteres, sem espaçamento e sem CRTL+G</em>.<br />

<strong>Comando:</strong><kbd>"/join #gatos4ever"</kbd>
</p>

![image](Imagens/Join1.png)

<p>Há cada novo integrande do canal, o administrador receberá uma mensagem, informando quantos entraram e seus nomes de usuário.<br />
E há cada canal em que tenha entrado, aparecerá ao lado do primeiro canal, como uma nova aba.
</p>

![image](Imagens/Join2.jpg)

<p> Sair dos canais que está vinculado: <kbd> "/join " </kbd>
</p>

#### 3.2 Tópicos - Temas do canal
<p> Para criar/modificar um tópico em um canal deve-se possuir privilégios de Administrador. <br />
O tópico poderia ser o tema de discussão que o grupo está discutindo no momento.<br />

<strong>Comando:</strong><kbd> "/topic gatosbrancos" </kbd>
</p>

![image](Imagens/Topic1.png)

#### 3.3 PART
<p> O comando PART serve para sair de um canal que esteja inserido. Quando sair não receberá uma mensagem, apenas a "aba" do canal sumirá.<br />
Mas quando algum integrante do canal sair, uma mensagem de saída aparecerá falando o nome de usuário. Ex: <strong>User1 has left #chuva<strong><br /><br />
<strong>Comando:</strong><kbd>"/part"</kbd>
</p>

#### 3.4 QUIT
<p> É usado sair do servidor e fechar o Chatzilla.<br />

<strong>Comando:</strong><kbd>"/quit"</kbd>
</p>

#### 3.5 LIST
<p> O comando LIST pode ser usados de duas formar para listar canais, tópicos e usuários.<br />

<strong>Comando:</strong><kbd>"/list"</kbd>. Será listado todos os canais, seus tópicos e quantidade de usuários em que estão inseridos.
</p>

![image](Imagens/List.png)

<p><strong>Comando:</strong> <kbd>"/list nomedocanal"</kbd>. Listará todos os integrantes do canal e seus nomes de usuário.
</p>

![image](Imagens/List1.png)

#### 3.6 NAMES
<p> O comando NAMES é bem semenhante ao LIST, porém só pode ser usado para um canal em específico e para mostrar os usuários.<br />
<strong>Comando:</strong><kbd>"/names nomedocanal"</kbd>
</p>

![image](Imagens/Names.png)

#### 3.7 INVITE
<p> O comando INVITE é usado pelos administradores de canais para convidar um usuário para entrar no canal.<br />
<strong>Comando:</strong><kbd>"/invite nomedousuário"</kbd><br /><br />

O usuário convidado receberá uma mensagem, ex: <strong>User1 (chatzilla@::1:55282) has invited you to [#gatos4ever]</strong>.<br />
Então o usuário convidado
terá que fazer um JOIN para entrar no canal, facilitado pela mensagem que mostra quem convidou e qual o nome do canal. <br />

</p>

![image](Imagens/Invite.png)

#### 3.8 KICK   
<p> O comando KICK é usado pelos administradores de canais para expulsar um usuário para do canal. Já foi relatado que esse comando é polêmico, pois
quando a pessoa é expulsa, pode-se mandar uma mensagem para ela.<br />
<strong>Comando:</strong><kbd>"/kick nomedousuário mensagemOpcional"</kbd><br /><br />

O usuário expulso receberá uma mensagem, ex: <strong>YOU (Fulano) have been booted from #gatos4ever by User1 (Não contribuiu para o grupo)</strong>.<br />
</p>

#### 3.9 WHOIS   
<p> O comando WHOIS é usado para demonstrar a descrição do usuário -> nickname, username, nome real, 
quais canais está inserido (canais em comum), qual servidor está hospedado e 
qual a localização.<br />
<strong>Comando:</strong><kbd>"/whois nickname"</kbd><br />

</p>

#### 3.10 WHOWAS   
<p> O comando WHOWAS é usado para demonstrar as informações do usuário ->  username, nome real, qual servidor está 
hospedado e qual horário em que foi iniciado (data/hora).<br />
<strong>Comando:</strong><kbd>"/whowas nickname"</kbd><br />

</p>



****