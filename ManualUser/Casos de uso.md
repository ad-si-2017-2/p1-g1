# Casos de Uso

## Caso de uso CDU01:
<p>
<strong>Escopo:</strong> Este caso de uso é referente ao comando JOIN<br />
<strong>Nível:</strong> Client <br />
<strong>Ator principal:</strong> Client chatzilla <br />
<strong>Pré-condições:</strong> Estar conectado ao servidor <br />
<strong>Pós-condições:</strong> Estar em um canal <br />
<strong>Entrada:</strong> /join #teste <br />
<strong>Cenário de sucesso(fluxo básico):</strong> - O usuário entra no canal #teste
<strong>Saída esperada(fluxo básico):</strong> You(nick) have joined #teste.<br />
</p>
    
## Caso de uso CDU02:
<p>
<strong>Escopo:</strong> Este caso de uso é referente ao comando JOIN<br />
<strong>Nível:</strong> Client<br />
<strong>Ator principal:</strong> Client chatzilla<br />
<strong>Pré-condições:</strong> Estar em um canal chamado #teste<br />
<strong>Pós-condições:</strong> Outro usuário entrar no canal <br />
<strong>Entrada:</strong> ----- <br />
<strong>Cenário de sucesso(fluxo básico):</strong> - Outro usuário entrou no canal e você foi notificado. <br />
<strong>Saída esperada(fluxo básico):</strong> Fulano(chatzilla@::ffff:127.0.0.1:55397) has joined #teste. <br />
</p>

## Caso de uso CDU03:
<p>
<strong>Escopo:</strong> Este caso de uso é referente ao comando TOPIC <br />
<strong>Nível:</strong> Client <br />
<strong>Ator principal:</strong> Cliente chatzilla <br />
<strong>Pré-condições:</strong> Estar em um canal chamado #adsi e ser operador dele.<br />
<strong>Pós-condições:</strong> O tópico do canal foi alterado para o tópico novo. <br />
<strong>Entrada:</strong> /topic novo topico<br />
<strong>Cenário de sucesso(fluxo básico):</strong> - O tópico foi alterado com sucesso<br />
<strong>Cenário(s) alternativo(s):</strong> - O tópic não foi alterado e uma mensagem de erro foi retornada<br />
<strong>Saída obtida(fluxo básico):</strong> wilan_ has changed the topic to “novo topico”.<br />
<strong>Saída obtida(fluxo alternativo):</strong> You need to be an operator in #adsi to do that.<br />
</p>

## Caso de uso CDU04:
<p>
<strong>Escopo:</strong> Este caso de uso é referente ao comando PART<br />
<strong>Nível:</strong> TClient<br />
<strong>Ator principal:</strong> Cliente chatzilla<br />
<strong>Pré-condições:</strong> Estar em um canal<br />
<strong>Pós-condições:</strong> Sair desse canal <br />
<strong>Entrada:</strong> /part<br />
<strong>Cenário de sucesso(fluxo básico):</strong> - Saiu do canal<br />
<strong>Cenário(s) alternativo(s):</strong> - Mensagem de erro pois não está em um contexto de canal<br />
<strong>Saída obtida(fluxo básico):</strong> Para quem ainda está no canal: wilan has left #adsi<br />
<strong>Saída obtida(fluxo alternativo):</strong> Command “leave” must be run in the context of a channel.<br />
</p>

## Caso de uso CDU05:
<p>
<strong>Escopo:</strong> Este caso de uso é referente ao comando QUIT<br />
<strong>Nível:</strong> Client<br />
<strong>Ator principal:</strong> Cliente chatzilla<br />
<strong>Pré-condições:</strong> Estar conectado ao servidor<br />
<strong>Pós-condições:</strong> Sair do servidor e fechar o client <br />
<strong>Entrada:</strong> /quit<br />
<strong>Cenário de sucesso(fluxo básico):</strong> - Saiu do servidor<br />
<strong>Saída obtida(fluxo básico):</strong> Saiu do servidor e fechou o chatzilla.<br />
</p>

## Caso de uso CDU06:
<p>
<strong>Escopo:</strong> Este caso de uso é referente ao comando LIST<br />
<strong>Nível:</strong> Client<br />
<strong>Ator principal:</strong> Cliente chatzilla<br />
<strong>Pré-condições:</strong> Estar conectado ao servidor.<br />
<strong>Pós-condições:</strong> Mostrar a lista de canais, quantos usuários tem e cada um e seu tópico <br />
<strong>Entrada:</strong> /list<br />
<strong>Cenário de sucesso(fluxo básico)</strong> - Mostrou a lista dos canais.<br />
<strong>Saída obtida(fluxo básico):</strong> #adsi   1   novo topico<br />
                            #adis   1   <br />
                            End of /LIST<br />
</p>

## Caso de uso CDU07:
<p>
<strong>Escopo:</strong> Este caso de uso é referente ao comando NAMES<br />
<strong>Nível:</strong> Client<br />
<strong>Ator principal:</strong> Cliente chatzilla<br />
<strong>Pré-condições:</strong> Estar conectado ao servidor<br />
<strong>Pós-condições:</strong> Mostrar a lista de todos os usuários de determinado canal<br />
<strong>Entrada:</strong> /names #adsi<br />
<strong>Cenário de sucesso(fluxo básico):</strong>
    - O comando é executado e mostra todos os usuários do canal especificado.<br />
<strong>Cenário(s) alternativo(s):</strong>
    - O canal especificado não existe e então uma mensagem de erro é retornada<br />
<strong>Saída obtida(fluxo básico):</strong> #adsi: @wilan wilan_ wilan__<br />
	                        #adsi: End of /NAMES list.<br />
<strong>Saída obtida(fluxo alternativo):</strong> The channel “#teste” does not exist.<br />
</p>
    
## Caso de uso CDU08:
<p>
<strong>Escopo:</strong> Este caso de uso é referente ao comando INVITE<br />
<strong>Nível:</strong> Client<br />
<strong>Ator principal:</strong> Cliente chatzilla<br />
<strong>Pré-condições:</strong> Estar em um canal, sendo o operador dele caso o canal seja apenas com convite.<br />
<strong>Pós-condições:</strong> Usuário especificado convidado para o canal.<br />
<strong>Entrada:</strong> /invite wilan__<br />
<strong>Cenário de sucesso(fluxo básico):</strong>
    - O usuário convidado é notificado do convite.<br />
<strong>Cenário(s) alternativo(s):</strong>
    - O usuário que realizou o comando não tem permissão de operador do canal que está apenas por convite.<br />
    - O nick passado pelo usuário que realizou o comando não existe.<br />
<strong>Saída obtida(fluxo básico):</strong> You have invited wilan__ to #adsi<br />
                            wilan (alexcrrr22@::1:60964) has invited you to [#adsi]<br />
<strong>Saída obtida(fluxo alternativo 1):</strong> You need to be an operator in #adsi to do that.<br />
<strong>Saída obtida(fluxo alternativo 2):</strong> The nickname “s” does not exist.<br />
</p>

## Caso de uso CDU09:
<p>
<strong>Escopo:</strong> Este caso de uso é referente ao comando KICK<br />
<strong>Nível:</strong> Client<br />
<strong>Ator principal:</strong> Cliente chatzilla<br />
<strong>Pré-condições:</strong> Estar em um canal e ser operador dele.<br />
<strong>Pós-condições:</strong> O usuário especificado no comando é expulso do canal.<br />
<strong>Entrada:</strong> /kick wilan_<br />
<strong>Cenário de sucesso(fluxo básico):</strong>
    - O usuário especificado é expulso do canal com sucesso.<br />
<strong>Cenário(s) alternativo(s):</strong>
    - O usuário especificado não existe<br />
    - O usuário que executou o comando não é operador do canal<br />
<strong>Saída obtida(fluxo básico):</strong> YOU (wilan_) have been booted from #adsi by wilan (wilan).<br />
<strong>Saída obtida(fluxo alternativo 1):</strong> The user “s” is not known to ChatZilla.<br />
<strong>Saída obtida(fluxo alternativo 2):</strong> You need to be an operator in You're not channel operator to do that.<br />
</p>

## Caso de uso CDU10:
<strong>Escopo:</strong> Este caso de uso é referente ao comando WHOIS<br />
<strong>Nível:</strong> Client<br />
<strong>Ator principal:</strong> Cliente chatzilla<br />
<strong>Pré-condições:</strong> Estar conectado ao servidor<br />
<strong>Pós-condições:</strong> Informações do usuário especificado no comando exibidas na tela. <br />
<strong>Entrada:</strong> /whois wilan<br />
<strong>Cenário de sucesso(fluxo básico):</strong>
    - As informações do usuário são exibidas na tela.<br />
<strong>Cenário(s) alternativo(s):</strong>
    - O usuário especificado não existe.<br />
<strong>Saída obtida(fluxo básico):</strong> wilan < wilancarlos@:1:60964 * :New Now Know How > “(null)”<br />
	                        wilan: attached to :1:60964 :Brasil, BR, SA “(null)”<br />
	                        End of WHOIS information for wilan<br />
<strong>Saída obtida(fluxo alternativo):</strong> teste There was no such nickname<br />
                                 teste End of WHOWIS<br />
</p>

## Caso de uso CDU11:
<p>
<strong>Escopo:</strong> Este caso de uso é referente ao comando WHOWAS<br />
<strong>Nível:</strong> Client<br />
<strong>Ator principal:</strong> Cliente chatzilla<br />
<strong>Pré-condições:</strong> Estar conectado ao servidor<br />
<strong>Pós-condições:</strong> Informações do usuário especificado no comando exibidas na tela. <br />
<strong>Entrada:</strong> /whowas wilan<br />
<strong>Cenário de sucesso(fluxo básico):</strong>
    - As informações foram exibidas corretamente.<br />
<strong>Cenário(s) alternativo(s):</strong>
    - O usuário especificado no comando não existia<br />
<strong>Saída obtida(fluxo básico):</strong> wilan wilancarlos :1:60964 * :New Now Know How<br />
	                        wilan: attached to localhost “Sun Oct 01 2017 14:06:48 GMT-0300 (Hora oficial do Brasil)”<br />
                            wilan End of WHOWAS<br />
<strong>Saída obtida(fluxo alternativo):</strong> s There was no such nickname<br />
                                 s End of WHOWAS<br />
</p>