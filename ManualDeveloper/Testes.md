# Testes em telnet

## Comandos NICK e USER
### Fluxo principal - Comando NICK e USER
<p> <strong> Entrada:</strong><br />
<kbd>NICK wilan <br />
USER wilan * * :Wilan Carlos da Silva</kbd><br />

<strong> Saída esperada: </strong><br />
<kbd>:localhost 001 wilan :Welcome to the Internet Relay Network wilan!wila@::1:45346<br />
:localhost 002 wilan localhost<br />
:localhost 003 wilan :Server createde on Sat Sep 30 2017 00:03:28 GMT+0000 (UTC)<br />
:localhost 004 wilan localhost irc-server-1.1.1 DOQRSZaghilopswz CFILMPQSbcefgijklmnopqrstvz bkloveqjfI<br />
:localhost 251 wilan :There are 1 users<br />
:localhost 254 wilan 0 :channels formed<br />
:localhost 375 wilan :- localhost Message of the day - <br />
:localhost 372 wilan :- Servidor IRC dos alunos da turma de Aplicações Distribuídas<br />
:localhost 376 wilan :End of MOTD command </kbd><br />

<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/CaminhoFelizNICK-USER.png)
    
### Fluxo alternativo - Comando NICK
<p> <strong> Entrada:</strong><kbd> NICK</kbd> <br />
<strong> Saída esperada:</strong><kbd>:localhost 431 * :No nickname given</kbd><br />
<strong>Saída obtida:</strong>
</p> 

![print](/Imagens/Testes/SemNICK.png)
    
### Fluxo alternativo 2 - Comando NICK
<p><strong>Entrada:</strong><kbd> NICK wilan</kbd> (já foi registrado um NICK com nome wilan) <br />
<strong>Saída esperada:</strong><kbd> :localhost 433 * :wilan</kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/NICKexistente.png)
    
### Fluxo alternativo - Comando USER
<p><strong>Entrada:</strong><kbd> USER wilanCarlos </kbd><br />
<strong>Saída esperada:</strong><kbd> :localhost 461 * USER :Not enough parameters</kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/comandoUSERimcompleto.png)

### Fluxo alternativo 2 - Comando USER
<p><strong>Entrada:</strong><kbd> USER wilan 7 * :Wilan </kbd> (modo que não existe)<br />
<strong>Saída esperada:</strong><kbd> :localhost 501 nickname :Unknown mode flag</kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/USERmodoInexistente.png)

### Fluxo alternativo 3 - Comando USER
<p><strong>Entrada:</strong><kbd> USER wilan * * :Wilan</kbd> (já foi executado um USER antes) <br />
<strong>Saída esperada:</strong><kbd> :localhost 462 * :Unauthorized command (already registered)</kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/USERjaRegistrado.png)
    
## Comando JOIN
### Fluxo principal 
<p><strong>Entrada:</strong><kbd>JOIN #documentos</kbd><br />
<strong>Saída esperada:</strong><br />
<kbd>:wila!wilan@::1:35500 JOIN #documentos<br />
:localhost MODE #documentos +ns<br />
:localhost 353 wila @ #documentos :@wila<br />
:localhost 366 wila #documentos :End of NAMES list</kbd><br />
            
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/JOINFeliz.png)
    
### Fluxo principal 2
<p><strong>Entrada:</strong><kbd>JOIN #documentos</kbd> (Outra pessoa no canal) <br />
<strong>Saída esperada integrante veterano:</strong><kbd> :alex!alex@::1:43212 JOIN #documentos</kbd> <br />
<strong>Saída esperada integrante calouro:</strong><br />
<kbd>:alex!alex@::1:43212 JOIN #documentos<br />
:localhost MODE #documentos +ns<br />
:localhost 353 alex @ #documentos :@wila alex<br />
:localhost 366 alex #documentos :End of NAMES list</kbd><br />
                        
<strong>Saídas obtidas:</strong>
</p>

![print](/Imagens/Testes/JOINusuárioNovo(PontodeVistadeAlguémjáNoCanal).png)

![print](/Imagens/Testes/JOINusuárioNovo(PontodeVistadeAlguémNovo).png)
    
### Fluxo alternativo
<p><strong>Entrada:</strong><kbd> JOIN #documentos</kbd> (Com o canal cheio)<br />
<strong>Saída esperada:</strong><kbd> :localhost 471 alex #documentos :Channel is full</kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/JOINemCanalCheio.png)


### Fluxo alternativo 2
<p> 
<strong>Entrada:</strong><kbd>JOIN #documentos</kbd> (Com o canal sendo apenas por convite)<br />
<strong>Saída esperada:</strong><kbd> :localhost 473 naliw #documentos :Invite only</kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/JOINCanalemConvite.png)

### Fluxo alternativo 3
<p> 
<strong>Entrada:</strong><kbd> JOIN #documentos </kbd> (Com o canal com senha)<br />
<strong>Saída esperada:</strong><kbd> :localhost 475 naliw #documentos :Wrong key</kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/JOINCanalcomChave.png)

### Fluxo alternativo 4
<p> 
<strong>Entrada:<strong><kbd> JOIN teste</kbd><br />
<strong>Saída esperada:<strong><kbd> :localhost 403 naliw teste :No such channel</kbd><br />
<strong>Saída obtida:<strong>
</p>

![print](/Imagens/Testes/JOINNomeCanalErrado.png)

## Comando QUIT
### Fluxo principal
<p> 
<strong>Entrada:</strong><kbd> QUIT</kbd><br />
<strong>Saída esperada: </strong><br />
<kbd>:naliw!naliw2@::1:53302 QUIT :Client quit<br />
Connection closed by foreign host.</kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/QUIT.png)

## Comando MODE USUARIO
### Fluxo principal
<p> 
<strong>Entrada:</strong><kbd>MODE wilan +i</kbd><br />
<strong>Saída esperada:</strong><kbd>:wilan MODE wilan :+i </kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/MODEUsuárioFeliz.png)

### Fluxo alternativo
<p> 
<strong>Entrada:</strong><kbd> MODE wilan 9 (Parâmentro errado)</kbd><br />
<strong>Saída esperada:</strong><kbd> :localhost 501 wilan :Unknown mode flag</kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/MODEUsuário(parametroErrado).png)

### Fluxo alternativo 2
<p> 
<strong>Entrada:</strong><kbd> MODE</kbd> <br />
<strong>Saída esperada:</strong><kbd> :localhost 461 wilan MODE :Not enough parameters</kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/MODEUsuário(parametroVazio).png)

### Fluxo alternativo 3
<p> 
<strong>Entrada:</strong><kbd> MODE liar </kbd>(nick inexistente)<br />
<strong>Saída esperada:</strong><kbd> :localhost 502 wilan :Cannot change mode for other users</kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/MODEUsuário(NICKinexistente).png)

## Comando MODE CANAL
### Fluxo principal
<p> 
<strong>Entrada:</strong><kbd> MODE #documentos +i </kbd><br />
<strong>Saída esperada:</strong> <kbd>:wilan!wilan@::1:51908 MODE #documentos +i</kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/MODEFeliz.png)

### Fluxo alternativo
<p> 
<strong>Entrada:</strong><kbd> MODE</kbd><br />
<strong>Saída esperada:</strong><kbd> :localhost 461 wilan MODE :Not enough parameters </kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/MODESemParâmetros.png)

### Fluxo alternativo 2
<p> 
<strong>Entrada:</strong><kbd> MODE #docmentos</kbd><br />
<strong>Saída esperada:</strong><kbd> :localhost 403 wilan #docmentos :No such channel </kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/MODENomeCanalErrado.png)

### Fluxo alternativo 3
<p> 
<strong>Entrada:</strong><kbd> MODE #documentos 9 </kbd><br />
<strong>Saída esperada:</strong><kbd> :localhost 501 wilan :Unknown MODE flag</kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/MODEParâmetroIncorreto.png)

## Comando PART
### Fluxo principal
<p> 
<strong>Entrada:</strong><kbd> PART #documentos</kbd><br />
<strong>Saída esperada:</strong><kbd> :wilan!wilan@::1:51908 PART #documentos </kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/PARTFeliz.png)

### Fluxo alternativo
<p> 
<strong>Entrada:</strong><kbd> PART</kbd><br />
<strong>Saída esperada:</strong><kbd> :localhost 461 wilan PART :Not enough parameters </kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/PARTSemParâmetros.png)

### Fluxo alternativo 2
<p> 
<strong>Entrada:</strong><kbd> PART #sala </kbd><br />
<strong>Saída esperada:</strong><kbd> :localhost 403 wilan #sala :No such channel </kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/PARTcanalinexistente.png)

### Fluxo alternativo 3
<p> 
<strong>Entrada:</strong><kbd>PART #teste </kbd><br />
<strong>Saída esperada:</strong><kbd>:localhost 442 wilan #teste :You are not on that channel </kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/PARTemCanalnãoInscrito.png)

## Comando TOPIC
### Fluxo principal
<p> 
<strong>Entrada:</strong><kbd> TOPIC #documentos :Documentação de testes</kbd><br />
<strong>Saída esperada:</strong><kbd> :wilan!wilan@::1:51908 TOPIC #documentos :Documentação de testes </kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/TOPICFeliz.png)

### Fluxo alternativo
<p> 
<strong>Entrada:</strong><kbd> TOPIC </kbd><br />
<strong>Saída esperada:</strong><kbd> :localhost 461 wilan TOPIC :Not enough parameters </kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/TOPICParâmetroVazio.png)

### Fluxo alternativo 2
<p> 
<strong>Entrada:</strong><kbd> TOPIC #docmentos :Documentação de testes </kbd><br />
<strong>Saída esperada:</strong><kbd> :localhost 442 wilan #docmentos :You're not on that channel </kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/TOPICnomeCanalErrado.png)


## Comando NAMES
### Fluxo principal
<p> 
<strong>Entrada:</strong><kbd> NAMES #adsi</kbd><br />
<strong>Saída esperada:</strong><kbd> :localhost 353 wilan @ #adsi :@teste <br />
:localhost 366 wilan #adsi :End of /NAMES list. </kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/NAMESFeliz.png)

### Fluxo alternativo
<p> 
<strong>Entrada:</strong><kbd> NAMES </kbd><br />
<strong>Saída esperada:</strong><kbd> :localhost 403 wilan undefined :No such channel</kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/NAMESsemParâmetro.png)

## Comando LIST
### Fluxo principal
<p> 
<strong>Entrada:</strong><kbd> LIST #adsi</kbd><br />
<strong>Saída esperada:</strong><kbd> :localhost 321 wilan Channel :Users Name <br />
:localhost 322 wilan #adsi 1 : <br />
:localhost 323 wilan :End of /LIST </kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/LISTcanal.png)

### Fluxo principal 2
<p> 
<strong>Entrada:</strong><kbd> LIST </kbd><br />
<strong>Saída esperada:</strong><kbd>:localhost 322 wilan #adsi 1 : <br />
:localhost 323 wilan :End of /LIST</kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/LIST.png)

### Fluxo alternativo
<p> 
<strong>Entrada:</strong><kbd> LIST #adi </kbd><br />
<strong>Saída esperada:</strong><kbd>:localhost 403 wilan #adi :No such channel</kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/LISTnomeCanalErrado.png)


## Comando INVITE
### Fluxo principal
<p> 
<strong>Entrada:</strong><kbd> INVITE wilan #adsi</kbd><br />
<strong>Saída esperada:</strong><kbd> :teste!teste@::1:40368 INVITE wilan :#adsi <br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/INVITEFeliz.png)

### Fluxo alternativo
<p> 
<strong>Entrada:</strong><kbd> INVITE testes #documentos </kbd><br />
<strong>Saída esperada:</strong><kbd>:localhost 401 wilan testes :No such nick/channel</kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/INVITEnomeCanalErrado.png)

### Fluxo alternativo 2
<p> 
<strong>Entrada:</strong><kbd> INVITE teste #testesEmandamento </kbd><br />
<strong>Saída esperada:</strong><kbd> </kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/INVITEcanalinexistente.png)

### Fluxo alternativo 3
<p> 
<strong>Entrada:</strong><kbd>INVITE</kbd><br />
<strong>Saída esperada:</strong><kbd>:localhost 461 wilan INVITE :Not enough parameters</kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/INVITEsemParâmetros.png)

## Comando KICK
### Fluxo principal
<p> 
<strong>Entrada:</strong><kbd>KICK #documentos teste :Chato!!!</kbd><br />
<strong>Saída esperada:</strong><kbd> :wilan!wilan@::1:40446 KICK #documentos teste :Chato!!! </kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/KICKFeliz.png)

### Fluxo alternativo 
<p> 
<strong>Entrada:</strong><kbd>KICK teste #documentos :Chato!!!</kbd><br />
<strong>Saída esperada:</strong><kbd> :localhost 403 wilan teste :No such channel </kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/KICKnomeCanalErrado.png)

### Fluxo alternativo 2
<p> 
<strong>Entrada:</strong><kbd>KICK #documentos testes :Chato!!!</kbd><br />
<strong>Saída esperada:</strong><kbd> :localhost 401 wilan #documentos :No such nick/channel. </kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/KICKnomeNickErrado.png)

### Fluxo alternativo 3
<p> 
<strong>Entrada:</strong><kbd>KICK</kbd><br />
<strong>Saída esperada:</strong><kbd> :localhost 461 wilan KICK :Not enough parameters </kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/KICKsemParâmetros.png)


## Comando PRIVMSG
### Fluxo principal
<p> 
<strong>Entrada:</strong><kbd>PRIVMSG #documentos :Ola a todos</kbd><br />
<strong>Saída esperada:</strong><kbd> :Teste!teste@::1:57364 PRIVMSG #documentos :Ola a todos </kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/PRIVMSGCanal.png)

### Fluxo principal 2
<p> 
<strong>Entrada:</strong><kbd>PRIVMSG wilan :Eu</kbd><br />
<strong>Saída esperada:</strong><kbd> :Teste!teste@::1:57364 PRIVMSG wilan :Eu </kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/PRIVMSGnick.png)

### Fluxo alternativo
<p> 
<strong>Entrada:</strong><kbd>PRIVMSG teste :Eu</kbd><br />
<strong>Saída esperada:</strong><kbd> :localhost 401 wilan teste :teste </kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/PRIVMSGnickErrado.png)

### Fluxo alternativo 2
<p> 
<strong>Entrada:</strong><kbd>PRIVMSG</kbd><br />
<strong>Saída esperada:</strong><kbd> :localhost 401 wilan undefined :undefined </kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/PRIVMSGsemParâmetros.png)


## Comando MOTD
### Fluxo principal
<p> 
<strong>Entrada:</strong><kbd>USER wilan * * :Wilan Carlos da Silva </kbd><br />
<strong>Saída esperada:</strong><kbd> :localhost 375 wilan :- localhost Message of the day - <br />
:localhost 372 wilan :- Servidor IRC dos alunos da turma de Aplicações Distribuídas <br />
:localhost 376 wilan :End of MOTD command </kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/MOTD.png)

## Comando WHO
### Fluxo principal
<p> 
<strong>Entrada:</strong><kbd>WHO #documentos </kbd><br />
<strong>Saída esperada:</strong><kbd> :localhost 352 wilan #documentos wilan ::1:57950 localhost wilan @H :0 Wilan Carlos da Silva<br />
:localhost 352 wilan #documentos teste ::1:57364 localhost Teste H :0 Teste<br />
:localhost 315 wilan #documentos </kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/WHOcanal.png)

### Fluxo alternativo
<p> 
<strong>Entrada:</strong><kbd>WHO</kbd><br />
<strong>Saída esperada:</strong><kbd>  </kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/WHOsemParâmetros.png)

## Comando PING
### Fluxo principal
<p> 
<strong>Entrada:</strong><kbd>PING :mensagem </kbd><br />
<strong>Saída esperada:</strong><kbd> :localhost PONG localhost :mensagem </kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/PING.png)


## Comando USERHOST
### Fluxo principal
<p> 
<strong>Entrada:</strong><kbd> USERHOST </kbd><br />
<strong>Saída esperada:</strong><kbd> :localhost 302 wilan :wilan=+wilan@::1:57950 </kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/USERHOST.png)


## Comando WHOIS
### Fluxo principal
<p> 
<strong>Entrada:</strong><kbd> WHOIS Teste </kbd><br />
<strong>Saída esperada:</strong><kbd> :localhost 311 wilan Teste teste ::1:57364 * :Teste <br />
:localhost 319 wilan Teste :#documentos<br />
:localhost 312 wilan Teste ::1:57364 :Brasil, BR, SA <br />
:localhost 318 wilan Teste :End of /WHOIS list. </kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/WHOISnick.png)

### Fluxo alternativo
<p> 
<strong>Entrada:</strong><kbd> WHOIS teste </kbd><br />
<strong>Saída esperada:</strong><kbd> :localhost 401 wilan teste :No such nick/channel. <br />
:localhost 318 wilan teste :End of /WHOIS list. </kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/WHOISnickErrado.png)

### Fluxo alternativo 2
<p> 
<strong>Entrada:</strong><kbd> WHOIS </kbd><br />
<strong>Saída esperada:</strong><kbd> :localhost 461 wilan WHOIS :Not enough parameters </kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/WHOISsemParâmetros.png)


## Comando WHOWAS
### Fluxo principal
<p> 
<strong>Entrada:</strong><kbd> WHOWAS Teste </kbd><br />
<strong>Saída esperada:</strong><kbd> :localhost 314 wilan Teste teste ::1:57364 * :Teste <br />
:localhost 312 wilan Teste localhost :Sun Oct 01 2017 16:22:44 GMT+0000 (UTC) <br />
:localhost 369 wilan Teste :End of WHOWAS </kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/WHOWASnick.png)

### Fluxo alternativo
<p> 
<strong>Entrada:</strong><kbd> WHOWAS teste </kbd><br />
<strong>Saída esperada:</strong><kbd> :localhost 406 wilan teste :There was no such nickname <br />
:localhost 369 wilan teste :End of WHOWAS </kbd><br />
<strong>Saída obtida:</strong>

</p>
![print](/Imagens/Testes/WHOWASnickErrado.png)

### Fluxo alternativo 2
<p> 
<strong>Entrada:</strong><kbd> WHOWAS </kbd><br />
<strong>Saída esperada:</strong><kbd> :localhost 461 wilan WHOWAS :Not enough parameters </kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/WHOWASsemParâmetros.png)


## Comando TIME
### Fluxo principal
<p> 
<strong>Entrada:</strong><kbd> TIME </kbd><br />
<strong>Saída esperada:</strong><kbd> :localhost 391 wilan localhost :Sun Oct 01 2017 16:48:29 GMT+0000 (UTC) </kbd><br />
<strong>Saída obtida:</strong>
</p>

![print](/Imagens/Testes/TIME.png)