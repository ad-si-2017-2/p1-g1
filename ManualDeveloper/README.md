# Manual do Desenvolvedor

## Histórico de Alterações

Data | Versão | Descrição | Autor
:---:|:------:|:---------:|:-------:
12/09/17 | 0.0.1 | Iniciar a descrição | Jerlianni Oliveira
13/09/17 | 0.0.2 | Mudança no estilo, ambiente Git | Jerlianni Oliveira
14/09/17 | 0.0.3 | Acréscimo de dois tópicos | Jerlianni Oliveira
15/09/17 | 0.1.1 | Descrição de cada mensagem suportada e como iniciar o servidor Telnet para testes | Jerlianni Oliveira
19/09/17 | 0.1.2 | Criação da primeira versão dos diagramas de Atividade e Sequencia | Wilan Carlos
24/09/17 | 0.1.3 | Criação da descrição das tecnologias utilizadas | Wilan Carlos
24/09/17 | 0.2.0 | Descrição de comandos suportados e pequenos/grandes ajustes | Jerlianni Oliveira

## Sumário
* [Introdução] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper/#1-introdu%C3%A7%C3%A3o)
    * [Público Alvo] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper/#11-p%C3%BAblico-alvo)
    * [Pré-requisitos técnicos] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper/#12-pr%C3%A9-requisitos-t%C3%A9cnicos)
* [Habilitar a plataforma de execução e ferramenta de teste] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper#2-habilitar-a-plataforma-de-execu%C3%A7%C3%A3o-e-ferramenta-de-teste)
* [Gitlab] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper#3-gitlab)
    *[Passos para o aquivo no gitlab] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper#31-passos-para-o-aquivo-no-gitlab)
* [Iniciando o servidor para teste no Telnet] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper/#4-iniciando-o-servidor-para-teste-no-telnet)
* [Lista de Mensagens Suportadas] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper/#5-lista-de-mensagens-suportadas)
    * [PASS] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper/#51-pass)
    * [USER] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper/#52-user)
    * [NICK] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper/#53-nick)
    * [JOIN] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper/#54-join)
    * [PART] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper/#55-part)
    * [MODE Usuário] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper#53-mode-usu%C3%A1rio)
    * [MODE Canal] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper#57-mode-canal-necess%C3%A1rio-ser-operador-da-rede-ou-pelo-menos-do-canal)
    * [Tópicos - Temas do canal] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper#58-t%C3%B3picos-temas-do-canal)
    * [NAMES] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper/#59-names)
    * [LIST] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper/#510-list)
    * [INVITE] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper/#511-invite)
    * [KICK] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper/#512-kick)
    * [QUIT] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper/#513-quit)
    * [PRIMVSG] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper#514-privmsg)
    * [MOTD] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper/#515-motd)
    * [WHO] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper/#516who)
    * [PING] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper/#517-ping)
    * [USERHOST] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper/#518-userhost)
    * [WHOIS] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper/#519-whois)
    * [WHOWAS] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper/#520-whowas)
    * [LUSERS] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper/#521-lusers)
    * [TIME] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper/#522-time)
* [Lista de Mensagens Não Suportadas] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper/#6-lista-de-mensagens-n%C3%A3o-suportadas)
* [Diagramas] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper/#7-diagramas)
    * [Atividade] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper/#71-diagrama-de-atividade)
    * [Sequencia] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper/#72-diagrama-de-sequ%C3%AAncia)
* [Tecnologias Utilizadas] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper#8-tecnologias-utilizadas)
    * [Protocolo TCP] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper#81-protocolo-tcp)
    * [Arquitetura Cliente Servidor] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper#82-arquitetura-cliente-servidor)
    * [Socket] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper#83-socket)
* [Lista de Referências] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper/#9-lista-de-refer%C3%AAncias)
* [Testes em telnet] (https://gitlab.com/ad-si-2017-2/p1-g1/tree/master/ManualDeveloper#10-testes-na-plataforma-telnet)

******
### 1. Introdução
<p>Esse <strong>manual</strong> fornecerá todas as informações necessárias para realizar com sucesso a integração com servidor IRC, e ter um melhor aproveitamento de suas funcionanlidades.
</p>

#### 1.1. Público Alvo
<p>As informações deste manual são destinadas aos desenvolvedores de software, analistas programadores e 
público em geral interessado em aprender como se integrar com servidores IRC. 
</p>

#### 1.2. Pré-requisitos técnicos
<p> O mecanismo de integração é simples, de modo que apenas conhecimentos 
intermediários em linguagem de programação JavaScript e para sockets poderá ajudar.
</p>

******
### 2. Habilitar a plataforma de execução e ferramenta de teste
<p>Para utilizar o servidor é necessário a instalação de uma plataforma de execução javascript que atuará do lado servidor, Node Js.<br />

A plataforma Nodejs pode ser encontrado no seu site oficial que pode ser encontrado pesquisando no <strong>Google</strong> ou
<a href="https://nodejs.org/en/download/">acessando aqui!</a><br />

No site tem os passos para fazer a instalação da plataforma, 
único requisito é saber qual seu sistema operacional e qual o seu processador (32 ou 64 bits).<br />

Para usar o <strong>Telnet</strong>, uma ferramenta importante para testes do protocolo, é necessário seguir 
alguns passos de habilitação (semelhante a uma instalação). <br /><br />

Se seu sistema operacional for <strong> Windows </strong>: 
<a href="https://www.redehost.com.br/duvidas/como-efetuar-o-comando-telnet--1471">acesse aqui!</a><br />
Se for <strong> Linux/Ubuntu/Debian </strong>:
<a href="http://ptcomputador.com/Networking/ftp-telnet/66590.html">acesse aqui!</a><br />
</p>

*****
### 3. Gitlab
<p> O código fonte de todo o programa será alocado no Gitlab. <br />
GitLab é um gerenciador de repositório Git para Web, ele também dispõem Wiki e rastreamento de Issues. 
É um produto da empresa GitLab B.V, sendo parecido com o GitHub mas difere por ser Open Source.
</p>

#### 3.1 Passos para o aquivo no gitlab

<p> Os códigos que serão usados para a integração com o servidor IRC estarão hospedados primáriamente no Gitlab, mas futuramente poderá ser passado para outras
planataformas que usam como base a estruturação de armazenamento Git.Será disponibilizado um video simples que mostra de forma abrangente como 
fazer integração com o Gitlab, mas será exemplificados os passos que podem ser seguidos para clonar o arquivo e como testá-lo. Primáriamente será necessário
a instalação do git em sua máquina. 

<strong>Siga as instruções de instalação do código Git </strong><a href="https://git-scm.com/book/pt-br/v1/Primeiros-passos-Instalando-Git"><strong>disponiveis aqui!</strong></a>

Com a instalação do git, entre no <strong>Prompt de Comando/Terminal</strong> e digite 
<kbd> git clone https://gitlab.com/ad-si-2017-2/p1-g1.git </kbd>,
com esse comando receberá uma cópia de quase todos os dados que o servidor do Git possui do projeto. 
A pasta clonada terá o nome <strong>p1-g1</strong> que será armazenado na pasta do usuário.
</p>

******
### 4. Iniciando o servidor para teste no Telnet

<p>Após clonar o projeto para sua máquina, inicie o servidor IRC usando estes comandos: </p>

![image](Imagens/Prompt.jpg)

<p>Se o protocolo funcionar, irá aparecer o nome da porta, assim como exemplificado na imagem. 
</p>

****
### 5. Lista de Mensagens Suportadas

#### 5.1. PASS
<p><kbd>PASS senhadesejada</kbd><br />
Mensagem de confirmação: <em>Comando PASS executado com sucesso.</em>
</p>

#### 5.2. USER
<p><kbd> USER username modo * :Nome real</kbd><br />
	
<strong>Modo: </strong><br />
    * 0 - sem modo; <br />
    * 2 - recebe o wallops (Mensagem Global);<br />
    * 8 - Invisivel.<br /><br />
Mensagem de confirmação: <em>Comando USER executado com sucesso.</em>
</p>

#### 5.3. NICK

<p><kbd>NICK nickname</kbd><br />

Mensagem de confirmação: <em>Comando NICK executado com sucesso.</em>
Para sair do servidor, use o comando <kbd> “QUIT”</kbd>.
</p>

#### 5.4. JOIN
<p>
Para entrar em um canal do chat é necessário usar o comando <kbd>“JOIN nomedocanal”</kbd><br /> Caso o canal tiver uma senha de segurança, use o comando “JOIN nomedocanal senha”.<br />
Para sair de todos os canais em que está vinculado - <kbd>"JOIN 0"</kbd>;<br />
Se entrar com um nome valido que não existe, você criará um canal e será operador do mesmo.<br />

Canal com nome válido: <em> #,&,!,+ (Inicial), 50 caracteres, sem espaçamento e sem CRTL+G</em>.<br />

Para mais informações deste comando, <a href="https://tools.ietf.org/html/rfc2812#section-3.2.1l">acesse aqui!</a>
</p>

#### 5.5. PART
<p>Comando padrão para sair de um canal - <kbd> “PART nomedocanal”</kbd>
</p>

#### 5.6. MODE Usuário
<p>Usando o comando <kbd> “MODE” </kbd> para usuário, o comando altera o modo com que ele se comporta nos canais. <br />
Para executar esse comando <kbd> "MODE username modo" </kbd> (Importante lembrar que os espaços são MUITO importantes para os comandos, pois se os omitir irá dar erro de sintaxe).<br />
Para retornar ao modo anterior, pode-se usar o mesmo comando. Para ver os modos disponiveis é "MODE username".<br />
</p>

#### 5.7. MODE Canal (Necessário ser operador da rede ou pelo menos do canal)
<p>Sintaxe: <kbd>"MODE nomedocanal +modo <parametro>"</kbd><br />
Pode-se usar mais de um modo e parametro no comando acrescentando o simbolo de + na sintaxe, e sinal de - para remover um modo existente.<br />

Ex: <kbd>"MODE #gatossaofofos +i"</kbd><br />
    <kbd>"MODE #gatossaofofos +o username"</kbd><br />
    <kbd>"MODE #gatossaofofos +ip"</kbd><br />
    <kbd>"MODE #gatossaofofos +v username -o username"</kbd><br />
    <kbd>"MODE #gatossaofofos +voI username username"</kbd><br /><br />
    
<em>Parâmetro</em>: senha, username de outro usuário e limite de usuários.<br />

Para mais informações <a href="https://tools.ietf.org/html/rfc2812#section-3.2.3">acesse aqui!</a><br /><br />

São dividos em dois tipos de modo: 
</p>

* Ativaveis e desativaveis
    * a - alterar o sinalizador de canal anônimo;
    * i - alterar o sinalizador do canal somente para convidados;
    * m - alterar o canal moderado;
    * n - alterar as mensagens não para canais de clientes no exterior;
    * q - alterar a bandeira do canal silencioso;
    * p - alterar o sinalizador do canal privado;
    * s - alterar a bandeira do canal secreto;
    * r - alterar o sinalizador do canal reop do servidor;
    * t - alterar o tópico ajustável pelo sinalizador do operador do canal apenas;

* Receber parâmetros
    * O - atribuir o status de "criador de canal";
    * o - dê / pegar o privilégio do operador do canal;
    * v - dê / pegar o privilégio de voz;
    * k - definir / remover a chave do canal (senha);
    * l - definir / remover o limite de usuário para o canal;
    * b - definir / remover a máscara de proibição para manter os usuários fora;
    * e - configurar / remover uma máscara de exceção para substituir uma máscara de proibição;
    * I - definir / remover uma máscara de convite para substituir automaticamente a bandeira de convite somente
    

#### 5.8. TOPIC
<p>Quando um canal não possui um tópico, os usuários receberão uma mensagem <kbd>“RPL_NOTOPIC”</kbd><br />
Para ver o tópico do canal use o comando <kbd>“TOPIC nomedocanal”</kbd><br /> Para criar ou modificar  um tópico  em um canal deve-se possuir privilégios de Administrador, 
o comando para adicionar/modificar tópicos de canais é <kbd>“TOPIC nomedocanal :topico que vc quer”</kbd><br /></p> 
<p>O nome do tópico pode ser separado por vírgula, ex: <kbd> “TOPIC #gatos4ever :Gatos são melhores que cães”</kbd><br />
Para alterar o nome do tópico pode-se usar o mesmo comando usado para criar o tópico.<br />
Para excluir o tópico, pode ser utilizado o comando <kbd>“TOPIC nomedocanal : ”</kbd><br /> Onde os dois pontos indicam o início do tópico para a exclusão.
</p>

#### 5.9. NAMES
<p><kbd> "NAMES nomedocanal"</kbd><br /> Serve para ver todos os usuários que podem ser vistos dentro do canal. <br />
<kbd>"NAMES"</kbd> Mostra todos os canais e seus usuários que podem ser vistos, e todos os usuários que não estão em canais e podem ser vistos. <br /><br />
<strong>Exemplo</strong><br />
<kbd> "NAMES nomedocanal1,nomedocanal2,nomedocanal3,nomedocanal4"</kbd>
</p>

#### 5.10. LIST
<p><kbd> "LIST nomedocanal"</kbd><br /> Serve para ver o status (tópicos) a ser vistos dentro do canal. <br />
<kbd>"LIST"</kbd> Mostra nome de todos os canais que podem ser vistos e seus  respectivos tópicos. <br /><br />
<strong>Exemplo</strong><br />
<kbd> "LIST nomedocanal1,nomedocanal2,nomedocanal3,nomedocanal4"</kbd>
</p>

#### 5.11. INVITE
<p><kbd> "INVITE nickname nomedocanal"</kbd><br /> Serve para convidar os usuários que possuem aquele nick o canal informado. <br /><br />
<strong>Exemplo</strong><br />
<kbd> "INVITE fulaninho nomedocanal"</kbd>
</p>

#### 5.12. KICK
<p><kbd> "KICK nomedocanal username"</kbd><br /> Serve para expulsar o usuário informado do canal. <br />
<p><kbd> "KICK nomedocanal username :mensagem de expulsão"</kbd><br /> Serve para mandar uma mensagem para o usuário expulso. <br /><br />
<strong>Exemplo</strong><br />
<kbd> "KICK nomedocanal fulano"</kbd><br />
<kbd> "KICK nomedocanal fulano :CHATO"</kbd>
</p>

#### 5.13. QUIT
<p><kbd> "QUIT"</kbd><br /> Serve para sair do servidor.
</p>

#### 5.14. PRIVMSG
<p><kbd> "PRIVMSG nickname OR nomedocanal :mensagem"</kbd><br /> Serve para enviar mensagens para usuários ou canais. <br /><br />
<strong>Exemplo</strong><br />
<kbd> "PRIVMSG fulaninho :reunião"</kbd><br />
<kbd> "PRIVMSG #gatos4ever :agendamento de reunião"</kbd>
</p>

#### 5.15. MOTD
<p><kbd> "MOTD"</kbd><br /> Serve para retornar a mensagem do dia.
</p>

#### 5.16. WHO
<p><kbd> "WHO nickname"</kbd><br /> Serve para retornar todas informações disponiveis do usuário.<br />
<kbd> "WHO nomedocanal"</kbd><br /> Retorna as informações dos usuários do canal.
</p>

#### 5.17. PING
<p><kbd> "PING mensagem"</kbd><br /> Serve para enviar uma mensagem ao servidor que retornará com um PONG e a mensagem que foi enviada.<br /><br />
<strong>Exemplo</strong><br />
<kbd> "PING teste"</kbd><br />
<kbd> "PONG teste"</kbd>
</p>

#### 5.18. USERHOST
<p>É um comando feito pelo Chatzilla para verificar os dados do usuário.
</p>

#### 5.19. WHOIS
<p><kbd> "WHOIS nickname"</kbd><br /> É usado para demonstrar a descrição do usuário -> nickname, username, nome real, 
quais canais está inserido (canais em comum), qual servidor está hospedado e qual a localização.
</p>

#### 5.20. WHOWAS
<p><kbd> "WHOWAS nickname"</kbd><br /> É usado para demonstrar as informações do usuário ->  username, nome real, qual servidor está 
hospedado e qual horário em que foi iniciado (data/hora).
</p>

#### 5.21. LUSERS
<p><kbd> "LUSERS"</kbd><br /> É usado para mostrar:
* Quantos usuários estão online no servidor                        
* Quantos operadores do irc estão online                        
* Quantos canais foram criados                        
* Qual o maior número de usuários registrados

</p>

#### 5.22. TIME
<p><kbd> "TIME"</kbd><br /> É usado para ver o horário do seridor.
</p>


****
### 6. Lista de Mensagens Não Suportadas

* VERSION
* LINKS
* CONNECT
* TRACE 
* ADMIM 
* INFO
* KILL
* ERROR

****

### 7. Diagramas

#### 7.1 Diagrama de Atividade

![DiagramaAtividade](https://uploaddeimagens.com.br/images/001/096/524/full/DiagramaAtividade1.0.png)

#### 7.2 Diagrama de Sequência

![DiagramaSequencia](https://uploaddeimagens.com.br/images/001/096/526/full/DiagramaSequencia1.0.png)

****

### 8. Tecnologias Utilizadas

#### 8.1. Protocolo TCP

<p> O protocolo TCP é um protocolo de nível 4 da camada de transporte do modelo OSI, 
é um protocolo orientado à conexão que permite a comunicação entre dois computadores 
estabelecendo uma conexão gerando um canal de comunicação antes que qualquer transmissão de dados seja realizada. <br />
Este protocolo garante que os dados sejam trafegados com versatilidade e robustez verificando
se os dados são enviados de forma correta, na sequência apropriada e sem erros pela rede.<br /><br />

O TCP introduz o conceito de porta associado a um serviço na camada de aplicação, garantindo assim que um servidor 
que oferece mais de um serviço por exemplo não envie informações a uma aplicação errada.
</p>

![CamadaTCPIP](https://uploaddeimagens.com.br/images/001/104/832/original/CamadaTCPIP.gif)
@abusar.org

#### 8.2. Arquitetura Cliente Servidor

<p> A arquitetura cliente servidor utiliza da camada de aplicação da arquitetura TCP/IP 
para realizar a troca de mensagem entre processos, o processamento é dividido entre uma aplicação cliente e uma aplicação servidor, 
onde o servidor atende a vários clientes e são centralizados possuindo endereço fixo e conhecidos, 
provendo aos clientes serviços requisitados. <br />
Por sua vez os clientes solicitam serviços/requisição ao servidor, onde dois clientes não se comunicam diretamente.<br /><br />

Para a comunicação entre os processos são utilizados protocolos para realizar a troca de mensagens
através da rede em uma comunicação fim-a-fim, onde os processos são identificados atráves do endereço 
IP(estação onde o processo se encontra) e a porta(identificador do processo na estação e controlado pelo SO).
</p>

![ArquiteturaClienteServidor](https://uploaddeimagens.com.br/images/001/104/861/full/ClienteServidor.png)


#### 8.3. Socket

<p> O socket é uma interface local, entre o processo de aplicação e o protocolo de transporte fim-a-fim (TCP) 
criada pela aplicação mas controlada pelo SO através de uma porta, no qual os processos de aplicação
podem enviar e receber mensagens de outros processos de aplicação locais ou remotos. Foi pensada com o intuito
de abstrair a camada de rede para que a aplicação não tenha que se preocupar com detalhes da pilha TCP/IP, dando assim
a abstração que os programas estão comunicando diretamente um com os outros, mas na pratica eles passam pela rede para troca
de mensagens.
</p>

![socket](https://uploaddeimagens.com.br/images/001/104/881/full/Socket.png)
![socket2](https://uploaddeimagens.com.br/images/001/104/885/full/Socket2.png)

<p> Como as portas e os Sockets são controlados pelo SO que oferece funções para utilização das mesmas e controla qual processo tem direito a escrever e ler em um determinado socket aberto, e apenas o Kernel do sistema por segurança pode abrir Sockets. 
Assim a maioria das linguares oferecem APIs para a utilização dos mesmos.
</p>

![socket3](https://blog.pantuza.com/uploads/658c71207341fbeaa74e5255d89f40cd4e6bbb78)

****

### 9. Lista de Referências

* [Registro de conexão] (https://tools.ietf.org/html/rfc2812#section-3.1)
* [Operação de canal] (https://tools.ietf.org/html/rfc2812#section-3.2)
* [Envio de mensagens] (https://tools.ietf.org/html/rfc2812#section-3.3)
* [Consultas ao servidor] (https://tools.ietf.org/html/rfc2812#section-3.4)
* [Consultas baseadas no usuário] (https://tools.ietf.org/html/rfc2812#section-3.6)
* [Variadas] (https://tools.ietf.org/html/rfc2812#section-3.7)

******

### 10. Testes na plataforma telnet

<p> Para ver como são os testes em telnet <a href="https://gitlab.com/ad-si-2017-2/p1-g1/blob/master/ManualDeveloper/Testes.md#testes-em-telnet">acesse aqui!</a>
</p>