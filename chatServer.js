// Load the TCP Library
net = require('net');

//TODO LIST
// Testar adaptação do comando nick


//chrome://chatzilla/content
//Abrir chatzilla na aba do firefox

// Keep track of the chat clients
var clients = []; // isso aqui é um array comum

var operators = [];

// Mapa de nicknames                                                                                   
// veja mais sobre array associativa em:                                                             
// https://www.w3schools.com/js/js_arrays.asp                                                          
// Array dos nicks dos clients
var nicks = {};

// Array dos dados de user dos clients
var users = new Object();

// Array dos canais
var channels = new Object();

//Data da criação do canal
var createDate = new Date();

var maxConnections = 0;

var connections = 0;

// Array dos modos de canal que são apenas de toggle
var channelMode = ["+a", "-a", "+i", "-i", "+m", "-m", "+n", "-n", "+q", "-q", "+p", "-p", "+s", "-s", "+t", "-t"];

// Array dos prefixos de canal
var prefixosCanal = ["#", "+", "&", "!"];

//Array dos prefixos de modo
var prefixosModo = ["+", "-"];

//Variável que guarda os dados do MOTD (Message Of The Day)
var msgDoDia = "Servidor IRC dos alunos da turma de Aplicações Distribuídas";

var serverName = "localhost";

// Start a TCP Server
var server = net.createServer(function(socket) {

  // Identify this client
  socket.name = socket.remoteAddress + ":" + socket.remotePort;

  // Put this new client in the list
  clients.push(socket);

  // Handle incoming messages from clients.
  socket.on('data', function(data) {
    analisar(data);
  });

  // Remove the client from the list when it leaves
  socket.on('end', function() {
    clients.splice(clients.indexOf(socket), 1);
    delete nicks[socket.nick];
    delete users[socket.nick];
  });

  socket.on('error', function() {
    socket.destroy();
  });


  function analisar(data) {

    // o método trim remove os caracteres especiais do final da string
    var mensagem = String(data).trim();
    // o método split quebra a mensagem em partes separadas p/ espaço em branco
    var args = mensagem.split(" ");
    console.log(mensagem);
    if (args[0] == "NICK") nick(args); //Feito
    else if (args[0] == "USER") user(args); //Feito
    else if (args[0] == "JOIN") join(args); //Feito
    else if (args[0] == "QUIT") quit(args); //Feito
    else if (args[0] == "MODE") mode(args); //Feito
    else if (args[0] == "PART") part(args); //Feito
    else if (args[0] == "TOPIC") topic(args); //Feito
    else if (args[0] == "NAMES") names(args); //Feito
    else if (args[0] == "LIST") list(args); //Feito
    else if (args[0] == "INVITE") invite(args); //Feito
    else if (args[0] == "KICK") kick(args); //Feito
    else if (args[0] == "PRIVMSG") privmsg(args); //Feito
    else if (args[0] == "MOTD") motd(args); //Feito
    else if (args[0] == "WHO") who(args); //Feito
    else if (args[0] == "PING") ping(args); //Feito
    else if (args[0] == "USERHOST") userhost(args); //Feito
    else if (args[0] == "WHOIS") whois(args); //Feito
    else if (args[0] == "WHOWAS") whowas(args); //Feito
    else if (args[0] == "LUSERS") lusers(args); //Feito
    else if (args[0] == "TIME") time(args); //Feito
    else if (args[0].length == 0) return;
    else socket.write(":" + serverName + " " + (socket.nick || socket.name) + " " + args[0] + " :Unknown  command\r\n");
  }


  function time(args) {
    socket.write(":" + serverName + " 391 " + socket.nick + " " + "localhost" + " " + ":" + new Date() + "\r\n");
    return;
  }

  function lusers(args) {
    socket.write(":" + serverName + " 251 " + socket.nick + " :" + "There are " + Object.keys(users).length + " users\r\n");
    socket.write(":" + serverName + " 252 " + socket.nick + " " + operators.length + " :IRC Operators online\r\n");
    socket.write(":" + serverName + " 253 " + socket.nick + " " + "0" + " :Unknown connection(s)\r\n");
    socket.write(":" + serverName + " 254 " + socket.nick + " " + Object.keys(channels) + " :channels formed\r\n");
    socket.write(":" + serverName + " 255 " + socket.nick + " " + ":I have " + clients.length + " clients and 1 servers\r\n");
    socket.write(":" + serverName + " 265 " + socket.nick + " " + clients.length + " " + maxConnections + " " + ":Current local users " + clients.length + ", max " + maxConnections + "\r\n");
    socket.write(":" + serverName + " 266 " + socket.nick + " " + clients.length + " " + maxConnections + " " + ":Current global users " + clients.length + ", max " + maxConnections + "\r\n");
    socket.write(":" + serverName + " 250 " + socket.nick + " " + ":Highest connection count: " + maxConnections + " (" + maxConnections + " clients" + ") " + "(" + connections + " connections " + " received" + ")\r\n");
  }


  function whowas(args) {
    if (!args[1]) {
      socket.write(":" + serverName + " 461 " + socket.nick + " WHOWAS " + ":Not enough parameters\r\n");
      return;
    }
    else if (!nicks[args[1]]) {
      socket.write(":" + serverName + " 406 " + socket.nick + " " + args[1] + " " + ":There was no such nickname\r\n");
      socket.write(":" + serverName + " 369 " + socket.nick + " " + args[1] + " " + ":End of WHOWAS\r\n");
      return;
    }

    /*Reply para quem fez o comando WHOWAS */
    socket.write(":" + serverName + " 314 " + socket.nick + " " + args[1] + " " + users[args[1]].user.username + " " + users[args[1]].name + " * " + ":" + users[args[1]].user.realname + "\r\n");
    socket.write(":" + serverName + " 312 " + socket.nick + " " + args[1] + " " + "localhost" + " " + ":" + createDate + "\r\n");
    socket.write(":" + serverName + " 369 " + socket.nick + " " + args[1] + " " + ":End of WHOWAS\r\n");

  }

  function whois(args) {
    if (!args[1]) {
      socket.write(":" + serverName + " 461 " + socket.nick + " WHOIS " + ":Not enough parameters\r\n");
      return;
    }
    else if (!nicks[args[1]]) {
      socket.write(":" + serverName + " 401 " + socket.nick + " " + args[1] + " :No such nick/channel.\r\n");
      socket.write(":" + serverName + " 318 " + socket.nick + " " + args[1] + " :End of /WHOIS list.\r\n");
      return;
    }

    /*Reply para quem enviou o comando whois */
    socket.write(":" + serverName + " 311 " + socket.nick + " " + args[1] + " " + users[args[1]].user.username + " " + users[args[1]].name + " " + "*" + " " + ":" + users[args[1]].user.realname + "\r\n");

    var isOnChannel = [];
    for (channel in channels) {
      if (channels[channel].clients[socket.nick] && channels[channel].opers[args[1]]) {
        isOnChannel.push("@" + channel);
      }
      else if (channels[channel].clients[socket.nick] && channels[channel].clients[args[1]] && !channels[channel].opers[args[1]]) {
        isOnChannel.push(channel);
      }
    }

    if (isOnChannel.length > 0)
      socket.write(":" + serverName + " 319 " + socket.nick + " " + args[1] + " " + ":" + isOnChannel.join(" ") + "\r\n");

    socket.write(":" + serverName + " 312 " + socket.nick + " " + args[1] + " " + users[args[1]].name + " :Brasil, BR, SA\r\n");
    socket.write(":" + serverName + " 318 " + socket.nick + " " + args[1] + " " + ":End of /WHOIS list.\r\n");
  }

  function ping(args) {
    socket.write(":" + serverName + " PONG localhost " + args[1] + "\r\n");
    return;
  }

  function userhost(args) {
    socket.write(":" + serverName + " 302 " + socket.nick + " :" + socket.nick + "=+" + socket.user.username + "@" + socket.name + "\r\n");
    return;
  }

  function who(args) {
    if (channels[args[1]]) { //Testa se a máscara passada existe
      var channelUsers = [];
      for (client in channels[args[1]].clients) {
        channelUsers.push(channels[args[1]].clients[client]);
      }

      channelUsers.forEach(function(client) {
        socket.write(":" + serverName + " 352 " + socket.nick + " " + args[1] + " " + client.user.username + " " + client.name + " " + "localhost" + " " + client.nick + " " + (channels[args[1]].opers[client.nick] ? "@" : "") + (client.mode.indexOf("a") == -1 ? "H" : "G") + " :" + "0" + " " + client.user.realname + "\r\n");
        console.log(":" + serverName + " 352 " + socket.nick + " " + args[1] + " " + client.user.username + " " + client.name + " " + "localhost" + " " + (channels[args[1]].opers[client.nick] ? "@" : "") + client.nick + " " + (client.mode.indexOf("a") == -1 ? "H" : "G") + " :" + "0" + " " + client.user.realname + "\r\n");
      });

      socket.write(":" + serverName + " 315 " + socket.nick + " " + args[1] + "\r\n");

    }
  }



  function nick(args) {

    if (!args[1]) { // se o segundo argumento for vazio                                         
      socket.write(":" + serverName + " 431 " + (socket.nick || "*") + " :No nickname given\r\n");
      return; // retorna da função, não executa depois daqui                                 
    }
    else if (nicks[args[1]]) { // procura o nickname no mapa                                  
      socket.write(":" + serverName + " 433 " + (socket.nick || "*") + " :" + args[1] + "\r\n");
      return;
    }
    else { // registra o nickname no mapa                                                          
      if (socket.nick && socket.user) { // o nickname já foi atribuído?   
        var trocaNick = socket.nick;

        /* Reply de mudança de nick para todos os usuários */
        for (user in users) {
          users[user].write(":" + socket.nick + "!" + socket.user.username + "@" + socket.name + " " + "NICK" + " " + ":" + args[1] + "\r\n");
        }

        // remove nickname atual no mapa                                               
        delete nicks[socket.nick];
        delete users[socket.nick];


        // inclui o nickname no mapa 	
        nicks[args[1]] = socket.name;


        // registra o nickname no objeto socket                                                
        socket.nick = args[1];

        /*Atualiza o array users */
        users[socket.nick] = socket;

        /*Atualiza os arrays de clients dos canais em que o usuário está*/
        for (channel in channels) {
          for (client in channels[channel].clients) {
            if (client == trocaNick) {
              delete channels[channel].clients[client];
              channels[channel].clients[socket.nick] = socket;
            }
          }
          for (client in channels[channel].opers) {
            if (client == trocaNick) {
              delete channels[channel].opers[client];
              channels[channel].opers[socket.nick] = socket;
            }
          }
        }

        return;
      }

      nicks[args[1]] = socket.name;
      socket.nick = args[1];
      users[socket.nick] = socket;
      boasVindas();
    }
  }

  function user(args) {

    //Reagrupa a entrada do usuário em uma única string separada por espaço
    var mensagem = args.join(" ");

    //Reparte a mensagem original em 2 partes
    var parte1 = mensagem.split(":")[0].trim().split(" ");
    //Tira da primeira parte o comando USER
    parte1.splice(0, 1);

    if (parte1.length < 3) { //Há parâmetros suficientes?
      socket.write(":" + serverName + " 461 " + (socket.nick || "*") + " USER :Not enough parameters\r\n");
      return;
    }
    else if (socket.user) {
      socket.write(":" + serverName + " 462 " + (socket.nick || "*") + " :Unauthorized command (already registered)\r\n");
      return;
    }


    var parte2 = mensagem.split(":")[1].trim();

    switch (parte1[1]) { //O segundo parâmetro é um comando de MODE válido?
      case "0":
        socket.mode = 0;
        break;
      case "+w":
        socket.mode = "w";
        break;
      case "+i":
        socket.mode = "i";
        break;
      case "+r":
        socket.mode = "r";
        break;
      case "+s":
        socket.mode = "s";
        break;
      case "*":
        socket.mode = "*";
        break;
      default:
        socket.write(":" + serverName + " 501 " + socket.nick + " :Unknown mode flag \r\n");
        return;
    }

    socket.user = { username: parte1[0], realname: parte2, password: socket.password, mode: [], isOper: false };
    socket.user.mode.push(socket.mode);
    boasVindas();
  }


  function mode(args) {
    if (!args[1]) {
      socket.write(":" + serverName + " 461 " + (socket.nick || "*") + " MODE :Not enough parameters\r\n");
      return;
    }
    else if (prefixosCanal.indexOf(args[1].charAt(0)) != -1) { //Testa se o segundo parâmetro que usuário entrou é um canal
      if (!channels[args[1]]) { //Testando se o canal existe
        socket.write(":" + serverName + " 403 " + (socket.nick || "*") + " " + args[1] + " :No such channel\r\n");
        return;
      }
      if (channels[args[1]].channelName.charAt(0) == "+") { //O canal não suporta modos?
        socket.write(":" + serverName + " 477 " + (socket.nick || "*") + " " + args[1] + " :Channel doesn't support modes\r\n");
        return;
      }
      if (args[2]) {
        if (args.length == 2) {
          /*Reply de comando MODE sómente com o nome do canal */
          socket.write(":" + serverName + " 324 " + socket.nick + " " + args[1] + " " + "+" + channels[args[1]].modes.join("") + "\r\n");
          return;
        }
        var params = Array.from(args[2]); //Transforma o(s) parâmetro(s) em um array
        args[2] = "" + params[0] + params[1];
        if (channelMode.indexOf(args[2]) != -1) { //O parâmetro é um dos modes de toggle?
          if (args[2].charAt(0) == "+") {
            channels[args[1]].modes.push(args[2].charAt(1));
          }
          else if (args[2].charAt(0) == "-") {
            channels[args[1]].modes.splice(channels[args[1]].modes.indexOf(args[2].charAt(1)), 1);
          }
          socket.write(":" + socket.nick + "!" + socket.user.username + "@" + socket.name + " " + "MODE" + " " + args[1] + " " + args[2] + "\r\n");
          return;
        }
        else {
          switch (args[2]) {
            case "+k":
              if (!channels[args[1]].channelKey && args[3]) channels[args[1]].channelKey = args[3];
              else if (!args[3] && !channels[args[1]].key) socket.write(":" + serverName + " 475 " + socket.nick + " " + args[1] + " " + ":Cannot join channel (+k)\r\n");
              else socket.write(":" + serverName + " 467 " + socket.nick + " " + args[1] + " :Channel key already set\r\n");
              break;
            case "-k":
              if (channels[args[1]].key) delete channels[args[1]].key; //Remove se tiver uma senha setada
              break;
            case "+l":
              if (!isNaN(Number.parseInt(args[3], 10))) channels[args[1]].limit = Number.parseInt(args[3], 10); //Eu testo se o terceiro parâmetro passado pelo usuário é mesmo um número
              break;
            case "-l":
              if (channels[args[1]].limit) delete channels[args[1]].limit;
              break;
            case "+O":
              if (channels[args[1]].clients[args[3]]) channels[args[1]].creator.push(args[3]);
              else socket.write(":" + serverName + " 441 " + socket.nick + " " + args[3] + " " + args[1] + " :They aren't on that channel\r\n");
              break;
            case "-O":
              if (channels[args[1]].creator[args[3]]) channels[args[1]].creator.splice(channel[args[1]].creator.indexOf(args[3]), 1);
              else if (!channels[args[1]].clients[args[3]]) socket.write(":" + serverName + " 441 " + socket.nick + " " + args[3] + " " + args[1] + " :They aren't on that channel\r\n");
              break;
            case "+o":
              if (channels[args[1]].clients[args[3]]) channels[args[1]].opers[args[3]] = args[3];
              else socket.write(":" + serverName + " 441 " + socket.nick + " " + args[3] + " " + args[1] + " :They aren't on that channel\r\n");
              break;
            case "-o":
              if (channels[args[1]].opers[args[3]]) delete channels[args[1]].opers[args[3]];
              else if (!channels[args[1]].clients[args[3]]) socket.write(":" + serverName + " 441 " + socket.nick + " " + args[3] + " " + args[1] + " :They aren't on that channel\r\n");
              break;
            case "+v":
              if (channels[args[1]].clients[args[3]]) channels[args[1]].clients[args[3]].voice = true;
              else socket.write(":" + serverName + " 441 " + socket.nick + " " + args[3] + " " + args[1] + " :They aren't on that channel\r\n");
              break;
            case "-v":
              if (channels[args[1]].clients[args[3]]) channels[args[1]].clients[args[3]].voice = false;
              else socket.write(":" + serverName + " 441 " + socket.nick + " " + args[3] + " " + args[1] + " :They aren't on that channel\r\n");
              break;
            case "+b":
              socket.write(":" + serverName + " 368 " + socket.nick + " " + args[1] + " " + ":End of Channel Ban List\r\n");
              break;
            default:
              socket.write(":" + serverName + " 501 " + socket.nick + " :Unknown MODE flag\r\n");
              return;
          }
          if (params.length > 2) { //Tem mais de um modo(junto) no parâmetro?
            var paramsRestantes = params.slice(2, params.length);
            var newModeParam = "" + args[2].charAt(0);
            for (var i = 0, len = paramsRestantes.length; i < len; i++) { //Vai adicionando os parâmetros restantes
              newModeParam = newModeParam + paramsRestantes[i];
            }
            analisar("MODE " + args[1] + " " + newModeParam + " " + (args[4] ? args[4] : args[3]));
          }
          else if (args[4]) { //Tem mais de um modo(separado) no parâmetro?
            if (prefixosModo.indexOf(args[4].charAt(0)) != -1) { //É um modo?
              analisar("MODE " + args[1] + " " + args[4] + " " + (args[5] ? args[5] : ""));
            }
            else { // Se não é um modo, é um outro parâmetro para o mesmo comando
              analisar("MODE " + args[1] + " " + args[2] + " " + args[4]);
            }
          }
        }
        for (client in channels[args[1]].clients) {
          channels[args[1]].clients[client].write(":irc://127.0.0.1 324 " + channels[args[1]].clients[client].nick + " " + args[1] + " " + "+" + channels[args[1]].modes.join("") + "\r\n");
        }
      }
    }
    else { // Se não é um canal
      if (socket.nick != args[1]) {
        socket.write(":" + serverName + " 502 " + socket.nick + " " + ":Cannot change mode for other users\r\n");
        return;
      }
      else if (args.length == 2) { //O usuário não entrou com o parâmetro do modo
        socket.write("a - user is flagged as away;\ni - marks a users as invisible;\nw - user receives wallops;\nr - restricted user connection;\no - operator flag;\nO - local operator flag;\ns - marks a user for receipt of server notices.\n")
        return;
      }

      var index;

      if (args[2]) {
        if (args[2].charAt(0) == ":") {
          var param = args[2].split(":");
          args[2] = param.join("");
        }
      }

      switch (args[2]) {
        case "+i":
          if (socket.user.mode.indexOf("i") == -1) { //O usuário ainda não possui este modo
            socket.user.mode.push("i");
          }
          break;
        case "-i":
          if (users[args[1]].mode.indexOf("i") != -1) { //Testando se o usuário possui este modo
            index = users[args[1]].mode.indexOf("i");
            users[args[1]].mode.splice(index, 1);
          }
          break;
        case "+w":
          if (users[args[1]].mode.indexOf("w") == -1) {
            users[args[1]].mode.push("w");
          }
          break;
        case "-w":
          if (users[args[1]].mode.indexOf("w") != -1) {
            index = users[args[1]].mode.indexOf("w");
            users[args[1]].mode.splice(index, 1);
          }
          break;
        case "+r":
          if (users[args[1]].mode.indexOf("r") == -1) {
            users[args[1]].mode.push("r");
            users[args[1]].isOper = false;
          }
          break;
        case "-o":
          users[args[1]].isOper = false;
          return;
        case "-O":
          users[args[1]].isOper = false;
          break;
        case "+s":
          if (users[args[1]].mode.indexOf("s") == -1) {
            users[args[1]].mode.push("s");
          }
          break;
        case "-s":
          if (users[args[1]].mode.indexOf("s") != -1) {
            index = users[args[1]].mode.indexOf("s");
            users[args[1]].mode.splice(index, 1);
          }
          break;
        default:
          socket.write(":" + serverName + " 501 " + socket.nick + " :Unknown mode flag \r\n");
          return;
      }
      /*Reply de mode de usuário */
      socket.write(":" + args[1] + " MODE " + args[1] + " :" + args[2] + "\r\n");
    }
  }

  function join(args) {

    if (!args[1]) { //Testa se tem parâmetros suficientes
      socket.write(":" + serverName + " 461 " + (socket.nick || "*") + " JOIN :Not enough parameters\n");
      return;
    }
    else if (args[1] == "0") { //Testa se o usuário enviou o parâmetro especial para que ele fosse removido de todos os canais

      for (channel in channels) { //Iterando dentro do array de canais
        if (channels[channel].clients[(socket.user ? socket.user.username : socket.name)]) { //Testando se o client é o mesmo client que requisitou sair de todos os canais
          analisar("PART " + channels[channel].channelName);
        }
      }
      return;
    }

    if (args[1].search(",") != -1) { //Testando se há mais um canal no parâmetro
      var params = args[1].split(",");
      args[1] = params[0];
    }

    if (args[2]) { //Tem parâmetros de key?
      if (args[2].search("," != -1)) { //Tem mais de um parâmetro de key?
        var paramKey = args[2].split(",");
        args[2] = paramKey[0];
        paramKey.splice(0, 1); //Tira a primeira key
      }
    };

    if (params) {
      params.splice(0, 1); //Tiro o primeiro canal pois esse é o que eu já adicionei o usuário
      if (params.length > 0) { //Se tem mais canais ainda
        params = params.join(",");
        if (paramKey) {
          if (paramKey.length > 0) {
            paramKey.join(",");
            analisar("JOIN " + params + " " + paramKey);
          }
        }
        else {
          analisar("JOIN " + params);
        }
      }
    }

    if (!channels[args[1]]) { //Se o canal ainda não existe ele pode ser criado
      switch (args[1].charAt(0)) {
        case "#":
          criarCanal(args[1]);
          break;
        case "&":
          criarCanal(args[1]);
          break;
        case "!":
          criarCanal(args[1]);
          break;
        case "+":
          criarCanal(args[1]);
          break;
        default:
          socket.write(":" + serverName + " 403 " + socket.nick + " " + args[1] + " :No such channel" + "\r\n");
          break;
      }
    }

    if (channels[args[1]]) {

      if (Object.keys(channels[args[1]].clients).length >= channels[args[1]].limit) { //Testa se o canal está cheio
        socket.write(":" + serverName + " 471 " + socket.nick + " " + args[1] + " :Channel is full" + "\r\n");
        return;
      }
      else if (channels[args[1]].modes.indexOf("i") != -1 && !channels[args[1]].invitedClients[socket.nick]) { //Testa se o canal é apenas por convite
        socket.write(":" + serverName + " 473 " + socket.nick + " " + args[1] + " :Invite only" + "\r\n");
        return;
      }
      else if (channels[args[1]].bannedClients[socket.nick]) { //Testa se o client está banido do canal
        socket.write(":" + serverName + " 474 " + socket.nick + " " + args[1] + " :You are banned" + "\r\n");
        return;
      }
      if (channels[args[1]].channelKey && args[2] != channels[args[1]].channelKey) { //Se o canal exige key e o usuário não entrou com a key certa
        socket.write(":" + serverName + " 475 " + socket.nick + " " + args[1] + " :Wrong key" + "\r\n");
        return;
      }



      /* Primeiro escreve para os outros clients no canal que o client em questão está entrando no canal */
      for (client in channels[args[1]].clients) {
        /*Avisa cada client do canal */
        channels[args[1]].clients[client].write(":" + socket.nick + "!" + socket.user.username + "@" + socket.name + " " + "JOIN " + args[1] + "\r\n");
      }

      /*Não alterar essa linha pois esse é um ponto chave do código para checagem de usuário em canais */
      channels[args[1]].clients[socket.nick] = socket; //Testa se o usuário cadastrou um user, um nick e se não tiver nenhum dos dois usa o socket.name mesmo
      /* */

      /* Confirmação de que o client entrou no canal com sucesso */
      socket.write(":" + socket.nick + "!" + socket.user.username + "@" + socket.name + " JOIN " + args[1] + "\r\n");

      /* Reply do tópico do canal se houver */
      if (channels[args[1]].topic)
        socket.write(":" + serverName + " 332 " + socket.nick + " " + args[1] + " :" + channels[args[1]].topic + "\r\n");

      /* Reply do modo do canal que acabou de ser criado */
      socket.write(":" + serverName + " " + "MODE" + " " + args[1] + " +ns\r\n");

      if (channels[args[1]].opers[socket.nick]) {
        /*Reply que coloca o usuário que criou o canal como operador */
        socket.write(":" + serverName + " " + "353" + " " + socket.nick + " " + "@" + " " + args[1] + " :@" + socket.nick + "\r\n");
        socket.write(":" + serverName + " 366 " + socket.nick + " " + args[1] + " :End of NAMES list\r\n");
      }
      else {
        /*Reply de usuários do canal */
        var channelUsers = [];
        for (client in channels[args[1]].clients) {
          if (channels[args[1]].opers[channels[args[1]].clients[client].nick]) channelUsers.push("@" + client);
          else channelUsers.push(client);
        }
        channelUsers = channelUsers.join(" ").trim();
        socket.write(":" + serverName + " 353 " + socket.nick + " @ " + args[1] + " :" + channelUsers + "\r\n");
        socket.write(":" + serverName + " 366 " + socket.nick + " " + args[1] + " :End of NAMES list\r\n");
      }
    }
  }

  function criarCanal(channelName) {
    if (channelName.length > 50 || channelName.search(" ") != -1 || channelName.search("^G") != -1 || channelName.search("^g") != -1 || channelName.search(":") != -1) {
      socket.write(":" + serverName + " 403 " + socket.nick + " " + channelName + " :No such channel\r\n");
      return;
    }
    else {
      switch (channelName.charAt(0)) {
        case "+": //Cria um canal que não suporta modos
          channels[channelName] = { limit: 10, clients: {}, inviteOnly: false, invitedClients: {}, bannedClients: {}, channelName: channelName, modes: [], creator: [], opers: {} };
          channels[channelName].modes.push("t");
          break;
        case "!": //Cria um canal com um operador com status de criador do canal
          channels[channelName] = { limit: 10, clients: {}, inviteOnly: false, invitedClients: {}, bannedClients: {}, channelName: channelName, opers: {}, modes: [], creator: [] };
          channels[channelName].creator.push(socket.nick);
          channels[channelName].opers[socket.nick] = socket;
          break;
        default: //Cria um canal padrão
          channels[channelName] = { limit: 10, clients: {}, inviteOnly: false, invitedClients: {}, bannedClients: {}, channelName: channelName, opers: {}, modes: [], creator: [] };
          channels[channelName].opers[socket.nick] = socket;
          channels[channelName].modes.push("b");
          break;
      }
    }
  }

  function boasVindas() {
    if (socket.nick && socket.user) {
      /* Replies para confirmar a conexão */
      socket.write(":" + serverName + " 001 " + socket.nick + " :Welcome to the Internet Relay Network " + socket.nick + "!" + socket.user.username + "@" + socket.name + "\r\n");
      socket.write(":" + serverName + " 002 " + socket.nick + " localhost\r\n");
      socket.write(":" + serverName + " 003 " + socket.nick + " :Server createde on " + createDate + "\r\n");
      socket.write(":" + serverName + " 004 " + socket.nick + " " + "localhost" + " " + "irc-server-1.1.1 " + "DOQRSZaghilopswz CFILMPQSbcefgijklmnopqrstvz bkloveqjfI" + "\r\n");
      server.getConnections(function(err, count) {
        socket.write(":" + serverName + " 251 " + socket.nick + " :There are " + count + " users\r\n");
        socket.write(":" + serverName + " 254 " + socket.nick + " " + Object.keys(channels).length + " :channels formed\r\n");
        motd();
      });
      if (clients.length > maxConnections) maxConnections = clients.length;
      connections++;
    };
  };

  function part(args) {

    if (!args[1]) {
      socket.write(":" + serverName + " 461 " + (socket.nick || "*") + " PART :Not enough parameters\r\n");
      return;
    }

    if (!channels[args[1]]) {
      socket.write(":" + serverName + " 403 " + (socket.nick || "*") + " " + args[1] + " :No such channel\r\n");
      return;
    }

    else if (!channels[args[1]].clients[(socket.nick)]) {
      socket.write(":" + serverName + " 442 " + (socket.nick || "*") + " " + args[1] + " :You are not on that channel\r\n");
      return;
    };

    if (args[2]) {
      if (args[2].charAt(0) == ":") {
        //Reagrupa a entrada do usuário em uma única string separada por espaço
        var mensagem = args.join(" ");
        var mensagemDeSaida = mensagem.split(":")[1].trim();
      }
    }

    var leavingUser = socket.nick;
    delete channels[args[1]].clients[socket.nick]; //Excluindo o client que quer sair do canal

    if (mensagemDeSaida)
      socket.write(":" + socket.nick + "!" + socket.user.username + "@" + socket.name + " PART " + args[1] + " :" + mensagemDeSaida + "\r\n")
    else
      socket.write(":" + socket.nick + "!" + socket.user.username + "@" + socket.name + " PART " + args[1] + "\r\n");

    if (Object.keys(channels[args[1]].clients).length > 0) { //Testando se ainda tem algum client no canal
      for (client in channels[args[1]].clients) { // Iterando dentro array dos clients do canal que o client saiu
        if (channels[args[1]].clients[client].nick != socket.nick) {
          if (mensagemDeSaida) // Se o usuário mandou uma mensagem de saída
            channels[args[1]].clients[client].write(":" + socket.nick + "!" + socket.user.username + "@" + socket.name + " PART " + args[1] + " :" + mensagemDeSaida + "\r\n");
          else // Mensagem padrão
            channels[args[1]].clients[client].write(":" + socket.nick + "!" + socket.user.username + "@" + socket.name + " PART " + args[1] + "\r\n");
        }
      }
    }
    else { //Se não tem client no canal
      delete channels[args[1]];
    }
  }

  function topic(args) {
    if (!args[1]) {
      socket.write(":" + serverName + " 461 " + (socket.nick || "*") + " TOPIC :Not enough parameters\n");
      return;
    }
    else if (!channels[args[1]] || !channels[args[1]].clients[socket.nick]) { //O canal passado existe? E se existe o client está presente nele?
      socket.write(":" + serverName + " 442 " + socket.nick + " " + args[1] + " :You're not on that channel\r\n");
      return;
    }
    else if (!channels[args[1]].opers[socket.nick]) { //O usuário existe e é um operador?
      socket.write(":" + serverName + " 482 " + socket.nick + " " + args[1] + " :You're not operator of that channel\r\n");
      return;
    }
    else if (!args[2]) { //O usuário passou um parâmetro de tópico?
      if (channels[args[1]].topic) //O canal possui tópico?
        socket.write(":" + serverName + " 332 " + socket.nick + " " + args[1] + " :" + channels[args[1]].topic + "\r\n");
      else
        socket.write(":" + serverName + " 332 " + socket.nick + " " + args[1] + " :Sem topico" + "\r\n");
      return;
    }
    else if (args[2].length == 1 && args[2].charAt(0) == ":") { //O usuário mandou uma string vazia?
      if (!channels[args[1]].topic) return; //Se o canal não possui tópico apenas retorna

      delete channels[args[1]].topic;
      return;
    }
    else if (args[2].charAt(0) == ":") {
      var topic = args.join(" ").trim().split(":")[1];
      channels[args[1]].topic = topic;
      for (client in channels[args[1]].clients) {
        channels[args[1]].clients[client].write(":" + socket.nick + "!" + socket.user.username + "@" + socket.name + " " + "TOPIC" + " " + args[1] + " :" + channels[args[1]].topic + "\r\n");
        console.log(":" + socket.nick + "!" + socket.user.username + "@" + socket.name + " " + "TOPIC" + " " + args[1] + " :" + channels[args[1]].topic + "\r\n");
      }
    }

  }


  function quit(args) {

    socket.write(":" + socket.nick + "!" + socket.user.username + "@" + socket.name + " " + "QUIT" + " " + ":Client quit\r\n");
    socket.end(); //fecha o socket da conexão do usuario
    return; // retorna da função, não executa depois daqui
  }

  function names(args) {
    var channelUsers = [];
    if (!channels[args[1]]) {
      socket.write(":" + serverName + " 403 " + socket.nick + " " + args[1] + " :No such channel\r\n");
      return;
    }
    for (client in channels[args[1]].clients) {
      if (channels[args[1]].opers[channels[args[1]].clients[client].nick]) channelUsers.push("@" + client);
      else channelUsers.push(client);
    }
    channelUsers = channelUsers.join(" ");
    socket.write(":" + serverName + " 353 " + socket.nick + " @ " + args[1] + " :" + channelUsers + "\r\n");


    socket.write(":" + serverName + " 366 " + socket.nick + " " + args[1] + " :End of /NAMES list.\r\n");
    return;
  }

  function list(args) {
    if (!args[1]) { //Comando LIST sem parâmetros
      if (Object.keys(channels).length > 0) { //Testa se existe algum canal criado
        for (channel in channels) {
          socket.write(":" + serverName + " 322 " + socket.nick + " " + channel + " " + Object.keys(channels[channel].clients).length + " " + ":" + (channels[channel].topic ? channels[channel].topic : "") + "\r\n");
        }
        socket.write(":" + serverName + " 323 " + socket.nick + " " + ":End of /LIST\r\n");
        return;
      }
    }

    if (!channels[args[1]]) {
      socket.write(":" + serverName + " 403 " + socket.nick + " " + args[1] + " :No such channel\r\n");
      return;
    }
    else {
      socket.write(":" + serverName + " 321 " + socket.nick + " Channel " + ":Users Name\r\n");
      socket.write(":" + serverName + " 322 " + socket.nick + " " + args[1] + " " + Object.keys(channels[args[1]].clients).length + " " + ":" + (channels[args[1]].topic ? channels[args[1]].topic : "") + "\r\n");
      socket.write(":" + serverName + " 323 " + socket.nick + " :End of /LIST\r\n");
      return;
    }

  }

  function invite(args) {
    if (args.length < 3) {
      socket.write(":" + serverName + " 461 " + (socket.nick || "*") + " INVITE :Not enough parameters\n");
      return;
    }
    else if (!nicks[args[1]]) { //Testa se o nick passado como parâmetro existe
      socket.write(":" + serverName + " 401 " + socket.nick + " " + args[1] + " :No such nick/channel\r\n");
      return;
    }
    else if (channels[args[2]]) { //Se o canal existe
      if (!channels[args[2]].clients[socket.nick]) { //Testa se o usuário está no canal passado como parâmetro
        socket.write(":" + serverName + " 442 " + socket.nick + " " + args[2] + " :You're not on that channel\r\n");
        return;
      }
      else if (!channels[args[2]].opers[socket.nick]) { //Testa se o canal tem o modo de somente convite e se o usuário é operador daquele canal
        socket.write(":" + serverName + " 482 " + socket.nick + " " + args[2] + " :You're not channel operator\r\n");
        return;
      }

      else if (channels[args[2]].clients[args[1]]) { //Testa se o convidado  já está no canal
        socket.write(":" + serverName + " 443 " + socket.nick + " " + args[1] + " " + args[2] + " :is already on channel\r\n");
        return;
      }

      /* Reply para quem enviou o convite */
      socket.write(":" + serverName + " 341 " + socket.nick + " " + args[1] + " " + args[2] + "\r\n");

      /*Reply para quem recebeu o convite */
      users[args[1]].write(":" + socket.nick + "!" + socket.user.username + "@" + socket.name + " " + "INVITE" + " " + args[1] + " :" + args[2] + "\r\n");

      /*Adiciona o convidado ao array de convidados */
      channels[args[2]].invitedClients[args[1]] = users[args[1]];

      return;
    }
  }

  function kick(args) {
    if (args.length < 3) {
      socket.write(":" + serverName + " 461 " + (socket.nick || "*") + " KICK :Not enough parameters\r\n");
      return;
    }
    else if(!nicks[args[2]]){
      socket.write(":" + serverName + " 401 " + socket.nick + " " + args[2] + " :No such nick/channel.\r\n");
      return;
    }

    else if (!channels[args[1]]) {
      socket.write(":" + serverName + " 403 " + socket.nick + " " + args[1] + " :No such channel\r\n");
      return;
    }
    else if (!channels[args[1]].clients[socket.nick]) {
      socket.write(":" + serverName + " 442 " + socket.nick + " " + args[1] + " :You're not on that channel\r\n");
      return;
    }
    else if (!channels[args[1]].opers[socket.nick]) {
      socket.write(":" + serverName + " 482 " + socket.nick + args[1] + " :You're not channel operator\r\n");
      return;
    }

    if (args[3]) {
      if (args[3].charAt(0) == ":") {
        var mensagem = args.join(" ");
        var mensagemDeExpulsao = mensagem.split(":")[1].trim();
      }
    }

    /*Reply pra quem está expulsando */
    socket.write(":" + socket.nick + "!" + socket.user.username + "@" + socket.name + " " + "KICK" + " " + args[1] + " " + args[2] + " :" + (mensagemDeExpulsao || args[2]) + "\r\n");

    /*Reply para quem está sendo expulsado */
    users[args[2]].write(":" + socket.nick + "!" + socket.user.username + "@" + socket.name + " " + "KICK" + " " + args[1] + " " + args[2] + " :" + (mensagemDeExpulsao || args[2]) + "\r\n");

    delete channels[args[1]].clients[args[2]];
    return;
  }

  function privmsg(args) {
    if (!nicks[args[1]] && !channels[args[1]]) {
      socket.write(":" + serverName + " 401 " + socket.nick + " " + args[1] + " :" + args[1] + "\r\n");
    }
    if (nicks[args[1]]) {
      args[2] = args.join(" ").split(":")[1];
      users[args[1]].write(":" + socket.nick + "!" + socket.user.username + "@" + socket.name + " PRIVMSG " + args[1] + " :" + args[2] + "\r\n");
      return;
    }
    else if (channels[args[1]]) {
      if (!channels[args[1]].clients[socket.nick]) {
        socket.write(":" + serverName + " 442 " + socket.nick + " " + args[1] + " " + ":" + "You are not on that channel\r\n");
        return;
      }
      args[2] = args.join(" ").split(":")[1];
      console.log(args[2]);
      for (client in channels[args[1]].clients) {
        if (channels[args[1]].clients[client].nick != socket.nick) channels[args[1]].clients[client].write(":" + socket.nick + "!" + socket.user.username + "@" + socket.name + " PRIVMSG " + args[1] + " :" + args[2] + "\r\n");
      }
    }
  }


  function motd(args) {
    if (msgDoDia.length > 0) {
      var motdStart = ":" + serverName + " 375 " + socket.nick + " :- " + "localhost" + " Message of the day - \r\n";
      var motd = ":" + serverName + " 372 " + socket.nick + " :- " + msgDoDia + "\r\n";
      var endOfMotd = ":" + serverName + " 376 " + socket.nick + " :End of MOTD command\r\n";
      socket.write(motdStart);
      socket.write(motd);
      socket.write(endOfMotd);
    }
    else {
      socket.write(":" + serverName + " 422 " + socket.nick + " :MOTD File is missing\r\n");
    }
  }


}).listen(6667, function() {
  // Put a friendly message on the terminal of the server.
  console.log("Chat server running at port 6667\n");
});
